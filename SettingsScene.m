//
//  SettingsScene.m
//  Tappin
//
//  Created by George Jones on 15/02/2014.
//  Copyright (c) 2014 Penguin George. All rights reserved.
//

#import "SettingsScene.h"
#import "MKStoreManager.h"

@implementation SettingsScene {
    NSNumberFormatter * _priceFormatter;
    
    SKLabelNode *_iapLabel;
    SKSpriteNode *_iapBuyButton;
    SKSpriteNode *_iapRestoreButton;
    SKLabelNode *_iapRestoreLabel;
    
    SKLabelNode *_colorModeLabel;
    SKSpriteNode *_colorModeButton;
    
    SKSpriteNode *_closeButton;
    SKLabelNode *_closeLabel;
    
    SKLabelNode *_titleLabel;
    
    BOOL useNightMode;
    BOOL nightMode;
}

- (id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"colStatus"] isEqualToString:@"DARK"]) {
            [self setBackgroundColor:[SKColor colorWithRed:.24 green:.24 blue:.24 alpha:1]];
            nightMode = YES;
        } else {
            [self setBackgroundColor:[SKColor colorWithRed:.93 green:.93 blue:.93 alpha:1]];
            nightMode = NO;
        }
        
        _priceFormatter = [[NSNumberFormatter alloc] init];
        [_priceFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
        [_priceFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        
        _iapBuyButton = [SKSpriteNode spriteNodeWithImageNamed:@"ButtonBg"];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            [_iapBuyButton setScale:0.8];
        } else {
            [_iapBuyButton setScale:0.5];
        }
        
        [_iapBuyButton setPosition:CGPointMake(self.size.width/2, self.size.height/2+90)];
        _iapLabel = [SKLabelNode labelNodeWithFontNamed:@"Corbert-Regular"];
        [_iapLabel setPosition:CGPointMake(self.parent.position.x, self.parent.position.y-15)];
        [_iapLabel setFontSize:36.0];
        _iapBuyButton.name = @"buyIap";
        _iapLabel.name = @"buyIap";
        [_iapBuyButton addChild:_iapLabel];
        [self addChild:_iapBuyButton];
        
        _iapRestoreButton = [SKSpriteNode spriteNodeWithImageNamed:@"ButtonBg"];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            [_iapRestoreButton setScale:0.8];
        } else {
            [_iapRestoreButton setScale:0.5];
        }
        
        [_iapRestoreButton setPosition:CGPointMake(self.size.width/2, self.size.height/2+30)];
        _iapRestoreLabel = [SKLabelNode labelNodeWithFontNamed:@"Corbert-Regular"];
        [_iapRestoreLabel setPosition:CGPointMake(self.parent.position.x, self.parent.position.y-15)];
        [_iapRestoreLabel setFontSize:36.0];
        _iapRestoreButton.name = @"restoreIAP";
        _iapRestoreLabel.name = @"restoreIAP";
        [_iapRestoreButton addChild:_iapRestoreLabel];
        [self addChild:_iapRestoreButton];

        
        
        _colorModeButton = [SKSpriteNode spriteNodeWithImageNamed:@"ButtonBg"];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            [_colorModeButton setScale:0.8];
        } else {
            [_colorModeButton setScale:0.5];
        }
        
        [_colorModeButton setPosition:CGPointMake(self.size.width/2, self.size.height/2-30)];
        _colorModeLabel = [SKLabelNode labelNodeWithFontNamed:@"Corbert-Regular"];
        [_colorModeLabel setPosition:CGPointMake(self.parent.position.x, self.parent.position.y-15)];
        [_colorModeLabel setFontSize:36.0];
        _colorModeButton.name = @"colorMode";
        _colorModeLabel.name = @"colorMode";
        [_colorModeButton addChild:_colorModeLabel];
        [self addChild:_colorModeButton];
        
        useNightMode = [[[NSUserDefaults standardUserDefaults] stringForKey:@"colStatus"] isEqualToString:@"DARK"] ? YES : NO;
        if (useNightMode) {
            [_colorModeLabel setText:NSLocalizedString(@"opt_DayMode", nil)];
        } else {
            [_colorModeLabel setText:NSLocalizedString(@"opt_NightMode", nil)];
        }
        
        _closeButton = [SKSpriteNode spriteNodeWithImageNamed:@"DoneButtonBg"];
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            [_closeButton setScale:0.8];
        } else {
            [_closeButton setScale:0.5];
        }
        [_closeButton setPosition:CGPointMake(self.size.width/2, self.size.height/2-90)];
        _closeLabel = [SKLabelNode labelNodeWithFontNamed:@"Corbert-Regular"];
        [_closeLabel setPosition:CGPointMake(self.parent.position.x, self.parent.position.y-15)];
        [_closeLabel setText:NSLocalizedString(@"opt_CloseMenu", nil)];
        [_closeLabel setFontColor:[UIColor darkGrayColor]];
        [_closeLabel setFontSize:36.0];
        _closeButton.name = @"closePrefs";
        _closeLabel.name = @"closePrefs";
        [_closeButton addChild:_closeLabel];
        [self addChild:_closeButton];
        
        _titleLabel = [SKLabelNode labelNodeWithFontNamed:@"Corbert-Regular"];
        [_titleLabel setText:NSLocalizedString(@"menu_Options", nil)];
        [_titleLabel setFontColor:[UIColor darkGrayColor]];
        [_titleLabel setPosition:CGPointMake(self.size.width/2, self.size.height/2 + self.size.height/4 + 20)];
        
        if (nightMode) {
            [_titleLabel setFontColor:[UIColor whiteColor]];
            [_closeLabel setFontColor:[UIColor whiteColor]];
        }
        
        [self addChild:_titleLabel];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            [_titleLabel setFontSize:48.0];
            [_iapBuyButton setPosition:CGPointMake(self.size.width/2, self.size.height/2+150)];
            [_iapRestoreButton setPosition:CGPointMake(self.size.width/2, self.size.height/2+50)];
            [_colorModeButton setPosition:CGPointMake(self.size.width/2, self.size.height/2-50)];
            [_closeButton setPosition:CGPointMake(self.size.width/2, self.size.height/2-150)];
        }
        
        [self configureStore];
    }
    return self;
}

- (void)restorePurchase {
    [[MKStoreManager sharedManager] restorePreviousTransactionsOnComplete:^{
        if ([MKStoreManager isFeaturePurchased:@"com.penguingeorge.games.Tappin.iap.RemoveAds"]) {
            [self removeAds];
        } else {
            UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Not purchased" message:@"You haven't purchased Ad Removal yet. The purchase can not be restored." delegate:self cancelButtonTitle:@"Okay" otherButtonTitles: nil];
            [errorAlert show];
        }
    } onError:nil];
}

- (void)configureStore {
    @try {
        if ([SKPaymentQueue canMakePayments]) {
            if ([MKStoreManager isFeaturePurchased:@"com.penguingeorge.games.Tappin.iap.RemoveAds"]) {
                [_iapRestoreLabel setText:NSLocalizedString(@"opt_IAPRestore", nil)];
                [_iapLabel setText:NSLocalizedString(@"opt_RemoveAds", nil)];
            } else {
                [_iapLabel setText:NSLocalizedString(@"opt_RemoveAds", nil)];
                [_iapRestoreLabel setText:NSLocalizedString(@"opt_IAPRestore", nil)];
            }
        } else {
            [_iapBuyButton removeFromParent];
        }
    }
    @catch (NSException *exception) {
        [Flurry logError:@"MK_IAP_SETUP_FAIL" message:@"SKPaymentQueue and MKStoreManager failed to follow process queue. See exception." exception:exception];
    }
}

- (void)purchaseIAP {
    @try {
        if([MKStoreManager isFeaturePurchased:@"com.penguingeorge.games.Tappin.iap.RemoveAds"]) {
            [self removeAds];
        } else {
            [[MKStoreManager sharedManager] buyFeature:@"com.penguingeorge.games.Tappin.iap.RemoveAds" onComplete:^(NSString* purchasedFeature, NSData* purchasedReceipt, NSArray* availableDownloads) {
                [self removeAds];
            } onCancelled:^{
                [Flurry logEvent:@"Transaction_Cancelled"];
            }];
        }
    }
    @catch (NSException *exception) {
        [Flurry logError:@"MK_IAP_PURCHASE_FAIL" message:@"MKStoreManager failed to follow process queue. See exception." exception:exception];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    
    if ([node.name isEqualToString:@"buyIap"]) {
        [self purchaseIAP];
    } else if ([node.name isEqualToString:@"colorMode"]) {
        [self changeColorMode];
    } else if ([node.name isEqualToString:@"closePrefs"]) {
        [self closeSettingsPanel];
    } else if ([node.name isEqualToString:@"restoreIAP"]) {
        [self restorePurchase];
    }
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions {
}

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
}

- (void)removeAds {
    [Flurry logEvent:@"Ads_Removed"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"HIDE" forKey:@"adStatus"];
    [defaults synchronize];

    UIAlertView *adsAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"av_IAPSuccess", nil) message:NSLocalizedString(@"av_IAPMessage", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"av_IAPButton", nil) otherButtonTitles: nil];
    [adsAlert show];
}

- (void)sendAchievementToGameCenter:(NSString *)ach {
    ViewController *vc = [[ViewController alloc] init];
    [vc submitAchievement:ach];
}

- (void)changeColorMode {
    if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"colStatus"] isEqualToString:@"DARK"]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"LIGHT" forKey:@"colStatus"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [_colorModeLabel setText:NSLocalizedString(@"opt_NightMode", nil)];
        [self setBackgroundColor:[SKColor colorWithRed:.93 green:.93 blue:.93 alpha:1]];
        [_titleLabel setFontColor:[UIColor darkGrayColor]];
        [_closeLabel setFontColor:[UIColor darkGrayColor]];
        useNightMode = NO;
    } else {
        [[NSUserDefaults standardUserDefaults] setObject:@"DARK" forKey:@"colStatus"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [_colorModeLabel setText:NSLocalizedString(@"opt_DayMode", nil)];
        [self setBackgroundColor:[SKColor colorWithRed:.24 green:.24 blue:.24 alpha:1]];
        [_titleLabel setFontColor:[UIColor whiteColor]];
        [_closeLabel setFontColor:[UIColor whiteColor]];
        useNightMode = YES;
        [self sendAchievementToGameCenter:kAchievementContrast];
    }
}

- (void)closeSettingsPanel {
    SKTransition *transition = [SKTransition pushWithDirection:SKTransitionDirectionDown duration:.8];
    NewGameScene *mainMenu = [[NewGameScene alloc] initWithSize:self.size];
    
    [self.scene.view presentScene:mainMenu transition:transition];
}

@end
