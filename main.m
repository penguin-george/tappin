//
//  main.m
//  Tappin
//
//  Created by George Jones on 2/5/14.
//  Copyright (c) 2014 George Jones. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[])
{
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
  }
}
