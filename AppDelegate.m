//
//  AppDelegate.m
//  Tappin
//
//  Created by George Jones on 2/5/14.
//  Copyright (c) 2014 George Jones. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "ACTReporter.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    _window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    ViewController *viewController = [[ViewController alloc] init];
    [_window setRootViewController:viewController];
    
    [_window setBackgroundColor:[UIColor whiteColor]];
    [_window makeKeyAndVisible];
    
    [Flurry setCrashReportingEnabled:YES];
    [Flurry startSession:@"REDACTED"];
    
    [ACTConversionReporter reportWithConversionID:@"REDACTED" label:@"REDACTED" value:@"0.000000" isRepeatable:NO];
    
    Reachability *reachStatus = [Reachability reachabilityWithHostname:@"http://connect.penguingeorge.net/services/net-test/pgnetservices-reachability.html"];
    reachStatus.reachableOnWWAN = NO;
    
    if ([reachStatus isReachableViaWiFi]) {
        [Flurry setSecureTransportEnabled:NO];
    } else {
        [Flurry setSecureTransportEnabled:YES];
    }
    
    @try {
        [[GameCenterManager sharedManager] setupManager];
        [[GameCenterManager sharedManager] setDelegate:self];
    } @catch (NSException *exception) {
        [Flurry logError:@"GAME_CENTER_FAILED" message:@"GameCenterManager failed to initialise." exception:exception];
    }
    
    return YES;
}

@end
