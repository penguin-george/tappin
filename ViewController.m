//
//  ViewController.m
//  Tappin
//
//  Created by George Jones on 2/5/14.
//  Copyright (c) 2014 George Jones. All rights reserved.
//

#import "ViewController.h"
#import "NewGameScene.h"

#define kAdMobID @"REDACTED";

@implementation ViewController

@synthesize gameCenterManager;
@synthesize currentScore;
@synthesize currentLeaderBoard;
@synthesize adView;

- (void)loadView {
    self.view  = [[SKView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame]];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    NSString *hasPermission = [[NSUserDefaults standardUserDefaults] stringForKey:@"analyticsPerms"];
    if ([hasPermission isEqualToString:@"no"]) {
        // Opted-out
    } else if ([hasPermission isEqualToString:@"yes"]) {
        // Opted-in
    } else {
        // Not asked
        //UIAlertView *permAlert = [[UIAlertView alloc] initWithTitle:@"Flurry Analytics" message:@"With your permission, anonymous usage information will be collected. This data includes app usage patterns, and is not identifiable to a specific device or user. The data is not given to any other third-party." delegate:self cancelButtonTitle:@"Opt-out" otherButtonTitles:@"Opt-in", nil];
        //[permAlert show];
        [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"analyticsPerms"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"analyticsPerms"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [Flurry setEventLoggingEnabled:NO];
        [Flurry setBackgroundSessionEnabled:NO];
        NSLog(@"<Tappin> Analytics Logging Disabled. Enable by deleting app and re-installing.");
    } else {
        [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"analyticsPerms"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    // Configure the view.
    SKView * skView = (SKView *)self.originalContentView;
    skView.showsFPS = NO;
    skView.showsNodeCount = NO;
    
    // Create and configure the scene.
    SKScene * scene = [NewGameScene sceneWithSize:skView.bounds.size];
    scene.scaleMode = SKSceneScaleModeAspectFill;
    
    Reachability *reachStatus = [Reachability reachabilityForInternetConnection];
    if ([reachStatus isReachable]) {
        if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"adStatus"] isEqualToString:@"HIDE"]) {
            NSLog(@"<Tappin> NoAdsEnabled");
        } else {
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                adView = [[ADBannerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-50, skView.frame.size.width, 0)];
                [adView setAlpha:0];
                [adView setDelegate:self];
                [self.view insertSubview:adView atIndex:0];
            } else if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
                adView = [[ADBannerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-66, skView.frame.size.width, 0)];
                [adView setAlpha:0];
                [adView setDelegate:self];
                [self.view insertSubview:adView atIndex:0];
            }
        }
    } else {
       // Don't initialise iAd!
    }
    
    // Present the scene.
    [skView presentScene:scene];
    
    //[self initiAdBanner];
    //[self initgAdBanner];
}

- (void)submitScore:(int)score {
    BOOL isAvailable = [[GameCenterManager sharedManager] checkGameCenterAvailability];
    
    if (isAvailable) {
        [[GameCenterManager sharedManager] saveAndReportScore:score leaderboard:kNormalLeaderboardID sortOrder:GameCenterSortOrderHighToLow];
    } else {
        NSLog(@"<Tappin> GameCenterManager reports Game Center is unavailable.");
    }
}

- (void)submitAchievement:(NSString *)achievementID {
    BOOL isAvailable = [[GameCenterManager sharedManager] checkGameCenterAvailability];
    
    if (isAvailable) {
        [[GameCenterManager sharedManager] saveAndReportAchievement:achievementID percentComplete:100 shouldDisplayNotification:YES];
    } else {
        NSLog(@"<Tappin> GameCenterManager reports Game Center is unavailable.");
    }
    
}

-(void)bannerViewDidLoadAd:(ADBannerView *)banner {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1];
    [banner setAlpha:1];
    [UIView commitAnimations];
}

-(void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1];
    [banner setAlpha:0];
    [UIView commitAnimations];
    [adView removeFromSuperview];
    [Flurry logError:@"AD_FAILED" message:@"iAd failed to receive an ad." error:error];
}

- (void)disableAds {
    [adView removeFromSuperview];

}
@end
