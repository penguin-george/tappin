//
//  NewGameScene.h
//  Tappin
//
//  Created by George Jones on 2/9/14.
//  Copyright (c) 2014 George Jones. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <iAd/iAd.h>

#import "Flurry.h"

@interface NewGameScene : SKScene <ADBannerViewDelegate>

@end