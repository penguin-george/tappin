//
//  MainScene.h
//  Tappin
//
//  Created by George Jones on 2/5/14.
//  Copyright (c) 2014 George Jones. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreMotion/CoreMotion.h>
#import "AppSpecificValues.h"
#import "ViewController.h"
#import "GameCenterManager.h"
#import "Flurry.h"


@interface MainScene : SKScene <SKPhysicsContactDelegate>

@property (strong) CMMotionManager* motionManager;

@end
