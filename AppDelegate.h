//
//  AppDelegate.h
//  Tappin
//
//  Created by George Jones on 2/5/14.
//  Copyright (c) 2014 George Jones. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iAd/iAd.h>
#import "GameCenterManager.h"
#import "Flurry.h"
#import "Reachability.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, GameCenterManagerDelegate, ADBannerViewDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
