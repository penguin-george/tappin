//
//  GameOverScene.m
//  Tappin
//
//  Created by George Jones on 10/02/2014.
//  Copyright (c) 2014 George Jones. All rights reserved.
//

#import "GameOverScene.h"
#import "MainScene.h"
#import "ViewController.h"

@implementation GameOverScene {
    SKLabelNode *_newGameLabel;
    SKSpriteNode *_newGameButton;
    SKLabelNode *_lastScoreLabel;
    SKLabelNode *_highScoreLabel;
    SKSpriteNode *_scoreBack;
    
    SKLabelNode *_infoLabel1;
    SKLabelNode *_infoLabel2;
    
    SKLabelNode *_copyrightLabel;
    
    SKSpriteNode *_character;
    
    SKSpriteNode *_twitterShare;
    SKSpriteNode *_facebookShare;
    
    GKScore *lastGKScore;
    
    BOOL nightMode;
}

- (id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        
        if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"colStatus"] isEqualToString:@"DARK"]) {
            [self setBackgroundColor:[SKColor colorWithRed:.24 green:.24 blue:.24 alpha:1]];
            nightMode = YES;
        } else {
            [self setBackgroundColor:[SKColor colorWithRed:.93 green:.93 blue:.93 alpha:1]];
            nightMode = NO;
        }
        
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
                _facebookShare = [SKSpriteNode spriteNodeWithImageNamed:@"Social_FacebookShare"];
                [_facebookShare setScale:0.15];
                [_facebookShare setPosition:CGPointMake(self.size.width/2+180, self.size.height/4)];
                _facebookShare.name = @"shareFacebook";
                [self addChild:_facebookShare];
            } else {
                _facebookShare = [SKSpriteNode spriteNodeWithImageNamed:@"Social_FacebookShare"];
                [_facebookShare setScale:0.08];
                [_facebookShare setPosition:CGPointMake(self.size.width/2+94, self.size.height/4-20)];
                _facebookShare.name = @"shareFacebook";
                [self addChild:_facebookShare];
            }
        }
        
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
                _twitterShare = [SKSpriteNode spriteNodeWithImageNamed:@"Social_TwitterShare"];
                [_twitterShare setScale:0.15];
                [_twitterShare setPosition:CGPointMake(self.size.width/2+96, self.size.height/4)];
                _twitterShare.name = @"shareTwitter";
                [self addChild:_twitterShare];
            } else {
                _twitterShare = [SKSpriteNode spriteNodeWithImageNamed:@"Social_TwitterShare"];
                [_twitterShare setScale:0.08];
                [_twitterShare setPosition:CGPointMake(self.size.width/2+50, self.size.height/4-20)];
                _twitterShare.name = @"shareTwitter";
                [self addChild:_twitterShare];
            }
        }
        
        _newGameButton = [SKSpriteNode spriteNodeWithImageNamed:@"ButtonBg"];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            [_newGameButton setScale:0.8];
            [_newGameButton setPosition:CGPointMake(self.size.width/2-86, self.size.height/4)];
        } else {
            [_newGameButton setScale:0.4];
            [_newGameButton setPosition:CGPointMake(self.size.width/2-44, self.size.height/4-20)];
        }
        
        
        _newGameLabel = [SKLabelNode labelNodeWithFontNamed:@"Corbert-Regular"];
        [_newGameLabel setText:NSLocalizedString(@"menu_PlayGame", nil)];
        [_newGameLabel setFontSize:42.0];
        [_newGameLabel setPosition:CGPointMake(self.parent.position.x, self.parent.position.y-15)];
        
        _newGameButton.name = @"newGame";
        _newGameLabel.name = @"newGame";
        
        [_newGameButton addChild:_newGameLabel];
        [self addChild:_newGameButton];
        
        [self setupCharacter];
        
        NSString *lastScore = [[NSUserDefaults standardUserDefaults] stringForKey:@"lastScore"];
        
        if ([lastScore integerValue] > [[NSUserDefaults standardUserDefaults] integerForKey:@"bestScore"]) {
            [[NSUserDefaults standardUserDefaults] setObject:lastScore forKey:@"bestScore"];
        }
        
         NSString *highScore = [[NSUserDefaults standardUserDefaults] stringForKey:@"bestScore"];
        
        _scoreBack = [SKSpriteNode spriteNodeWithImageNamed:@"BackPanel"];
        [_scoreBack setZPosition:1];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            [_scoreBack setScale:0.4];
        } else {
            [_scoreBack setScale:0.78];
        }
        
        [_scoreBack setPosition:CGPointMake(self.size.width/2, self.size.height/2)];
        [self addChild:_scoreBack];
        
        _lastScoreLabel = [[SKLabelNode alloc] initWithFontNamed:@"Corbert-Regular"];
        [_lastScoreLabel setPosition:CGPointMake(_scoreBack.frame.origin.x + _scoreBack.size.width/2, self.size.height/2 + 10)];
        [_lastScoreLabel setFontSize:54];
        _lastScoreLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
        [_lastScoreLabel setFontColor:[UIColor whiteColor]];
        [_lastScoreLabel setText:[NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:[lastScore integerValue]]]];
        [_lastScoreLabel setZPosition:2];
        [self addChild:_lastScoreLabel];
        
        _highScoreLabel = [[SKLabelNode alloc] initWithFontNamed:@"Corbert-Regular"];
        [_highScoreLabel setPosition:CGPointMake(_scoreBack.frame.origin.x + _scoreBack.size.width/2, (self.size.height/2-90))];
        [_highScoreLabel setFontSize:54];
        _highScoreLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
        [_highScoreLabel setFontColor:[UIColor whiteColor]];
        [_highScoreLabel setText:[NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:[highScore integerValue]]]];
        [_highScoreLabel setZPosition:2];
        [self addChild:_highScoreLabel];
        
        _infoLabel1 = [[SKLabelNode alloc] initWithFontNamed:@"Corbert-Regular"];
        [_infoLabel1 setPosition:CGPointMake(_scoreBack.frame.origin.x + _scoreBack.size.width/2, (self.size.height/2+60))];
        [_infoLabel1 setFontColor:[UIColor whiteColor]];
        _infoLabel1.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
        [_infoLabel1 setText:[NSString stringWithFormat:NSLocalizedString(@"info_Score", nil)]];
        [_infoLabel1 setZPosition:2];
        [self addChild:_infoLabel1];
        
        _infoLabel2 = [[SKLabelNode alloc] initWithFontNamed:@"Corbert-Regular"];
        [_infoLabel2 setPosition:CGPointMake(_scoreBack.frame.origin.x + _scoreBack.size.width/2, (self.size.height/2-40))];
        [_infoLabel2 setFontColor:[UIColor whiteColor]];
        _infoLabel2.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
        [_infoLabel2 setText:[NSString stringWithFormat:NSLocalizedString(@"info_BestScore", nil)]];
        [_infoLabel2 setZPosition:2];
        [self addChild:_infoLabel2];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy"];
        NSString *yearString = [formatter stringFromDate:[NSDate date]];
        
        _copyrightLabel = [SKLabelNode labelNodeWithFontNamed:@"Corbert-Regular"];
        [_copyrightLabel setText:[NSString stringWithFormat:@"Copyright © %@ Penguin George", yearString]];
        [_copyrightLabel setFontColor:[UIColor darkGrayColor]];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            [_copyrightLabel setFontSize:20.0];
            [_copyrightLabel setPosition:CGPointMake(self.size.width/2, 92)];
        } else {
            [_copyrightLabel setFontSize:12.0];
            [_copyrightLabel setPosition:CGPointMake(self.size.width/2, 60)];
        }
        
        if (nightMode) {
            [_copyrightLabel setFontColor:[UIColor whiteColor]];
        }
        
        [self addChild:_copyrightLabel];
        
        if ([lastScore integerValue] == 0) {
            [self sendAchievementToGameCenter:kAchievementZeroPoints];
        } else if ([lastScore integerValue] >= 100) {
            [self sendAchievementToGameCenter:kAchievementCentury];
        } else if ([lastScore integerValue] >= 1000) {
            [self sendAchievementToGameCenter:kAchievementThousandPoints];
        } else if ([lastScore integerValue] >= 1000000) {
            [self sendAchievementToGameCenter:kAchievementMillionPoints];
        }
        
        [self sendScoreToGameCenter];
        
        NSDictionary *eventParams =
        [NSDictionary dictionaryWithObjectsAndKeys:
         lastScore, @"Last_Score",
         nil];
        [Flurry logEvent:@"Score_Submitted" withParameters:eventParams];
    }
    return self;
}

- (void)setupCharacter {
    _character = [SKSpriteNode node];
    
    int randCharacter = arc4random() % 6;
    switch (randCharacter) {
        case 0:
            _character = [SKSpriteNode spriteNodeWithImageNamed:@"PlayerObj"];
            break;
        case 1:
            _character = [SKSpriteNode spriteNodeWithImageNamed:@"PlayerObj_Blue"];
            break;
        case 2:
            _character = [SKSpriteNode spriteNodeWithImageNamed:@"PlayerObj_Yellow"];
            break;
        case 3:
            _character = [SKSpriteNode spriteNodeWithImageNamed:@"PlayerObj_Purple"];
            break;
        case 4:
            _character = [SKSpriteNode spriteNodeWithImageNamed:@"PlayerObj_Blue"];
            break;
        case 5:
            _character = [SKSpriteNode spriteNodeWithImageNamed:@"PlayerObj_Brown"];
            break;
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [_character setScale:0.75];
        [_character setPosition:CGPointMake(self.size.width/2, (self.size.height/2) + (self.size.height/4) + 50)];
    } else {
        [_character setScale:0.5];
        [_character setPosition:CGPointMake(self.size.width/2, (self.size.height/2) + (self.size.height/4) + 30)];
    }
    
        [self addChild:_character];
    
    [self runAnimations];
    [NSTimer scheduledTimerWithTimeInterval:8 target:self selector:@selector(runAnimations) userInfo:nil repeats:YES];
}

- (void)runAnimations {
    [_character runAction:[SKAction sequence:@[
                                               [SKAction moveByX:-5 y:20 duration:1.0],
                                               [SKAction moveByX:5 y:-20 duration:1.0],
                                               [SKAction moveByX:0 y:20 duration:1.0],
                                               [SKAction moveByX:0 y:-20 duration:1.0],
                                               [SKAction moveByX:5 y:20 duration:1.0],
                                               [SKAction moveByX:-5 y:-20 duration:1.0],
                                               [SKAction moveByX:0 y:20 duration:1.0],
                                               [SKAction moveByX:0 y:-20 duration:1.0]]]];
}

- (void)sendAchievementToGameCenter:(NSString *)ach {
    ViewController *vc = [[ViewController alloc] init];
    [vc submitAchievement:ach];
}

- (void)sendScoreToGameCenter {
    ViewController *vc = [[ViewController alloc] init];
    [vc submitScore:(int)[[NSUserDefaults standardUserDefaults] integerForKey:@"lastScore"]];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    
    if ([node.name isEqualToString:@"newGame"]) {
        SKTransition *transition = [SKTransition pushWithDirection:SKTransitionDirectionUp duration:.8];
        MainScene *main = [[MainScene alloc] initWithSize:self.size];
        [self.scene.view presentScene:main transition:transition];
        //[self showGameCenter];
    } else if ([node.name isEqualToString:@"shareTwitter"]) {
        SLComposeViewController *twitterSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        NSString *lastScore = [[NSUserDefaults standardUserDefaults] stringForKey:@"lastScore"];
        
        [twitterSheet setInitialText:[NSString stringWithFormat:@"I got %@ on Tappin! Download it now for iPhone and iPad on the App Store. https://itunes.apple.com/us/app/tappin/id819608071?mt=8&at=10lsXw #TappinGame", lastScore]];
        [twitterSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"Post Sucessful");
                    [Flurry logEvent:@"Game_Shared_Twitter"];
                    break;
                default:
                    break;
            }
        }];
        UIViewController *vc = self.view.window.rootViewController;
        [vc presentViewController:twitterSheet animated:YES completion:nil];
    } else if ([node.name isEqualToString:@"shareFacebook"]) {
        SLComposeViewController *facebookSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        NSString *lastScore = [[NSUserDefaults standardUserDefaults] stringForKey:@"lastScore"];
        
        [facebookSheet setInitialText:[NSString stringWithFormat:@"I got %@ on Tappin! Download it now for iPhone and iPad on the App Store: https://itunes.apple.com/us/app/tappin/id819608071?mt=8&at=10lsXw", lastScore]];
        [facebookSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"Post Sucessful");
                    [Flurry logEvent:@"Game_Shared_Facebook"];
                    break;
                default:
                    break;
            }
        }];
        UIViewController *vc = self.view.window.rootViewController;
        [vc presentViewController:facebookSheet animated:YES completion:nil];
    }
}

@end
