//
//  GameOverScene.h
//  FlapFlap
//
//  Created by George Jones on 10/02/2014.
//  Copyright (c) 2014 George Jones. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <GameKit/GameKit.h>
#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import "Flurry.h"

@interface GameOverScene : SKScene  {
    
}

@property (nonatomic, retain) NSString *lastScore;

@end
