//
//  NewGameScene.m
//  Tappin
//
//  Created by George Jones on 2/9/14.
//  Copyright (c) 2014 George Jones. All rights reserved.
//

#import "NewGameScene.h"
#import "MainScene.h"
#import "SettingsScene.h"

@implementation NewGameScene {
    SKSpriteNode *_button;
    SKSpriteNode *_character;
    SKSpriteNode *_title;
    
    SKLabelNode *_copyrightLabel;
    
    SKSpriteNode *_newGameButton;
    SKLabelNode *_newGameLabel;
    
    SKSpriteNode *_shareButton;
    SKLabelNode *_shareLabel;
    
    SKSpriteNode *_prefButton;
    SKLabelNode *_prefLabel;
    
    BOOL nightMode;
}

- (id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"colStatus"] isEqualToString:@"DARK"]) {
            [self setBackgroundColor:[SKColor colorWithRed:.24 green:.24 blue:.24 alpha:1]];
            nightMode = YES;
        } else {
            [self setBackgroundColor:[SKColor colorWithRed:.93 green:.93 blue:.93 alpha:1]];
            nightMode = NO;
        }
        
        //[self setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"PlayerObj"]]];
        
        _newGameButton = [SKSpriteNode spriteNodeWithImageNamed:@"ButtonBg"];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            [_newGameButton setPosition:CGPointMake(self.size.width/2, self.size.height/4+150)];
        } else {
            [_newGameButton setPosition:CGPointMake(self.size.width/2, self.size.height/4+135)];
        }
        
        _newGameLabel = [SKLabelNode labelNodeWithFontNamed:@"Corbert-Regular"];
        [_newGameLabel setText:NSLocalizedString(@"menu_PlayGame", nil)];
        [_newGameLabel setFontSize:48.0];
        [_newGameLabel setFontColor:[UIColor whiteColor]];
        
        [_newGameLabel setPosition:CGPointMake(self.parent.position.x, self.parent.position.y-15)];
        _newGameButton.name = @"newGame";
        _newGameLabel.name = @"newGame";
        [_newGameButton addChild:_newGameLabel];
        [self addChild:_newGameButton];
        
        _shareButton = [SKSpriteNode spriteNodeWithImageNamed:@"ButtonBg"];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            [_shareButton setPosition:CGPointMake(self.size.width/2, self.size.height/4+50)];
        } else {
            [_shareButton setPosition:CGPointMake(self.size.width/2, self.size.height/4+75)];
        }
        
        _shareLabel = [SKLabelNode labelNodeWithFontNamed:@"Corbert-Regular"];
        [_shareLabel setText:NSLocalizedString(@"menu_ShareGame", nil)];
        [_shareLabel setFontSize:48.0];
        [_shareLabel setFontColor:[UIColor whiteColor]];
        [_shareLabel setPosition:CGPointMake(self.parent.position.x, self.parent.position.y-15)];
        _shareButton.name = @"shareGame";
        _shareLabel.name = @"shareGame";
        [_shareButton addChild:_shareLabel];
        [self addChild:_shareButton];
        
        _prefButton = [SKSpriteNode spriteNodeWithImageNamed:@"ButtonBg"];
        
        [_prefButton setPosition:CGPointMake(self.size.width/2, self.size.height/4-50)];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            [_prefButton setPosition:CGPointMake(self.size.width/2, self.size.height/4-50)];
        } else {
            [_prefButton setPosition:CGPointMake(self.size.width/2, self.size.height/4+15)];
        }
        
        _prefLabel = [SKLabelNode labelNodeWithFontNamed:@"Corbert-Regular"];
        [_prefLabel setText:NSLocalizedString(@"menu_Options", nil)];
        [_prefLabel setFontSize:48.0];
        [_prefLabel setFontColor:[UIColor whiteColor]];
        [_prefLabel setPosition:CGPointMake(self.parent.position.x, self.parent.position.y-15)];
        _prefButton.name = @"gamePrefs";
        _prefLabel.name = @"gamePrefs";
        [_prefButton addChild:_prefLabel];
        [self addChild:_prefButton];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy"];
        NSString *yearString = [formatter stringFromDate:[NSDate date]];
        
        _copyrightLabel = [SKLabelNode labelNodeWithFontNamed:@"Corbert-Regular"];
        [_copyrightLabel setText:[NSString stringWithFormat:@"Copyright © %@ Penguin George", yearString]];
        [_copyrightLabel setFontColor:[UIColor darkGrayColor]];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            [_copyrightLabel setFontSize:20.0];
            [_copyrightLabel setPosition:CGPointMake(self.size.width/2, 92)];
        } else {
            [_copyrightLabel setFontSize:12.0];
            [_copyrightLabel setPosition:CGPointMake(self.size.width/2, 66)];
        }
        
        if (nightMode) {
            [_copyrightLabel setFontColor:[UIColor whiteColor]];
        }
        
        [self addChild:_copyrightLabel];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            [_newGameButton setScale:0.8];
            [_shareButton setScale:0.8];
            [_prefButton setScale:0.8];
        } else {
            [_newGameButton setScale:0.5];
            [_shareButton setScale:0.5];
            [_prefButton setScale:0.5];
        }
        
        [self setupCharacter];

    }
    return self;
}

- (void)setupCharacter {
    _character = [SKSpriteNode node];
    
    int randCharacter = arc4random() % 6;
    switch (randCharacter) {
        case 0:
            _character = [SKSpriteNode spriteNodeWithImageNamed:@"PlayerObj"];
            break;
        case 1:
            _character = [SKSpriteNode spriteNodeWithImageNamed:@"PlayerObj_Blue"];
            break;
        case 2:
            _character = [SKSpriteNode spriteNodeWithImageNamed:@"PlayerObj_Yellow"];
            break;
        case 3:
            _character = [SKSpriteNode spriteNodeWithImageNamed:@"PlayerObj_Purple"];
            break;
        case 4:
            _character = [SKSpriteNode spriteNodeWithImageNamed:@"PlayerObj_Blue"];
            break;
        case 5:
            _character = [SKSpriteNode spriteNodeWithImageNamed:@"PlayerObj_Brown"];
            break;
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [_character setScale:0.75];
    } else {
        [_character setScale:0.5];
    }
    
    [_character setPosition:CGPointMake(self.size.width/2, (self.size.height/2) + (self.size.height/4)+ 35)];
    [self addChild:_character];
    
    _title = [SKSpriteNode spriteNodeWithImageNamed:@"Logo3"];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [_title setScale:0.50];
    } else {
        [_title setScale:0.20];
    }
    
    [_title setPosition:CGPointMake(self.size.width/2, self.size.height/2+85)];
    [self addChild:_title];
    
    [self runAnimations];
    [NSTimer scheduledTimerWithTimeInterval:8 target:self selector:@selector(runAnimations) userInfo:nil repeats:YES];
}

- (void)runAnimations {
    [_character runAction:[SKAction sequence:@[
                                           [SKAction moveByX:-5 y:20 duration:1.0],
                                           [SKAction moveByX:5 y:-20 duration:1.0],
                                           [SKAction moveByX:0 y:20 duration:1.0],
                                           [SKAction moveByX:0 y:-20 duration:1.0],
                                           [SKAction moveByX:5 y:20 duration:1.0],
                                           [SKAction moveByX:-5 y:-20 duration:1.0],
                                           [SKAction moveByX:0 y:20 duration:1.0],
                                           [SKAction moveByX:0 y:-20 duration:1.0]]]];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    
    if ([node.name isEqualToString:@"newGame"]) {
        [Flurry logEvent:@"New_Game_Started"];
        
        SKTransition *transition = [SKTransition pushWithDirection:SKTransitionDirectionUp duration:.8];
        MainScene *main = [[MainScene alloc] initWithSize:self.size];
        [self.scene.view presentScene:main transition:transition];
    } else if ([node.name isEqualToString:@"shareGame"]) {
        [self showShare];
    } else if ([node.name isEqualToString:@"gamePrefs"]) {
        SKTransition *transition = [SKTransition pushWithDirection:SKTransitionDirectionUp duration:.8];
        SettingsScene *main = [[SettingsScene alloc] initWithSize:self.size];
        [self.scene.view presentScene:main transition:transition];
    }
}

- (void)showShare {
    UIViewController *vc = self.view.window.rootViewController;
    
    NSString *textToShare = @"Check out Tappin on the App Store and try to be my highscore! #TappinGame";
    NSArray *itemsToShare = @[textToShare];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll]; //or whichever you don't need
    [vc presentViewController:activityVC animated:YES completion:nil];
}


@end
