//
//  ViewController.h
//  Tappin
//
//  Created by George Jones on 2/5/14.
//  Copyright (c) 2014 George Jones. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>
#import <GameKit/GameKit.h>
#import "AppSpecificValues.h"
#import "Flurry.h"
#import "GameCenterManager.h"
#import <iAd/iAd.h>


@interface ViewController : UIViewController <GameCenterManagerDelegate, GKGameCenterControllerDelegate, UINavigationControllerDelegate, ADBannerViewDelegate, UIAlertViewDelegate> {
    GameCenterManager *gameCenterManager;
    int64_t  currentScore;
    NSString* currentLeaderBoard;
}

@property (nonatomic, retain) ADBannerView *adView;

@property (nonatomic, retain) GameCenterManager *gameCenterManager;
@property (nonatomic, assign) int64_t currentScore;
@property (nonatomic, retain) NSString* currentLeaderBoard;

- (void)submitAchievement:(NSString *)achievementID;
- (void)submitScore:(int)score;
- (void)disableAds;

@end
