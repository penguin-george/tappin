//
//  SettingsScene.h
//  Tappin
//
//  Created by George Jones on 15/02/2014.
//  Copyright (c) 2014 Penguin George. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <StoreKit/StoreKit.h>
#import "Flurry.h"
#import "NewGameScene.h"
#import "ViewController.h"

@interface SettingsScene : SKScene <SKPaymentTransactionObserver, SKProductsRequestDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) SKProduct *product;
@property (strong, nonatomic) NSString *productID;


@end
