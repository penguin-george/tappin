//
//  MainScene.m
//  Tappin
//
//  Created by George Jones on 2/5/14.
//  Copyright (c) 2014 George Jones. All rights reserved.
//

#import "MainScene.h"
#import "NewGameScene.h"
#import "GameOverScene.h"
#import "Player.h"
#import "Obstacle.h"

static const uint32_t kPlayerCategory = 0x1 << 0;
static const uint32_t kPipeCategory = 0x1 << 1;
static const uint32_t kGroundCategory = 0x1 << 2;
static const uint32_t kSideCategory = 0x1 << 3;

//static const CGFloat kGravity = -10;
static const CGFloat kDensity = 1.15;
//static const CGFloat kMaxVelocity = 400;

static const CGFloat kPipeSpeed = 1.2;
static const CGFloat kPipeWidth = 54.0;
static const CGFloat kPipeGap = 3;
static const CGFloat kPipeFrequency = 0.4;

static const NSInteger kNumLevels = 20;

static const CGFloat randomFloat(CGFloat Min, CGFloat Max){
    return floor(((rand() % RAND_MAX) / (RAND_MAX * 1.0)) * (Max - Min) + Min);
}

@implementation MainScene {
    Player *_player;
    SKNode *_lPanel;
    SKNode *_rPanel;
    SKLabelNode *_scoreLabel;
    SKSpriteNode *_scoreBack;
    
    SKNode *_infoNode;
    SKLabelNode *_infoLabel1;
    SKLabelNode *_infoLabel2;
    SKSpriteNode *_infoIconLeft;
    SKSpriteNode *_infoIconRight;
    
    NSInteger _score;
    NSTimer *_pipeTimer;
    NSTimer *_scoreTimer;
    SKAction *_pipeSound;
    SKAction *_punchSound;
    
    BOOL gameStarted;
    BOOL fasterAt100;
    BOOL hasPlayedIntro;
    BOOL nightMode;
}

- (id)initWithSize:(CGSize)size
{
    if (self = [super initWithSize:size]) {
        _score = 0;
        
        srand((time(nil) % kNumLevels)*10000);
        
        if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"colStatus"] isEqualToString:@"DARK"]) {
            [self setBackgroundColor:[SKColor colorWithRed:.24 green:.24 blue:.24 alpha:1]];
            nightMode = YES;
        } else {
            [self setBackgroundColor:[SKColor colorWithRed:.93 green:.93 blue:.93 alpha:1]];
            nightMode = NO;
        }
        
        //self.motionManager = [[CMMotionManager alloc] init];
        //[self.motionManager startAccelerometerUpdates];
        
        [self.physicsWorld setGravity:CGVectorMake(0, 0)];
        [self.physicsWorld setContactDelegate:self];
        
        _scoreLabel = [[SKLabelNode alloc] initWithFontNamed:@"Corbert-Regular"];
        [_scoreLabel setPosition:CGPointMake(self.size.width/2, self.size.height-50)];
        [_scoreLabel setFontColor:[UIColor whiteColor]];
        [_scoreLabel setText:[NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:_score]]];
        [_scoreLabel setZPosition:2];
        [self addChild:_scoreLabel];

        _scoreBack = [SKSpriteNode spriteNodeWithImageNamed:@"ScoreBg"];
        [_scoreBack setZPosition:1];
        [_scoreBack setScale:0.4];
        [_scoreBack setPosition:CGPointMake(_scoreLabel.position.x, _scoreLabel.position.y+_scoreBack.size.height/4)];
        [self addChild:_scoreBack];
        
        [self setupPlayer];
        
        gameStarted = NO;

        _infoNode = [SKNode node];
        _infoLabel1 = [[SKLabelNode alloc] initWithFontNamed:@"Corbert-Regular"];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            [_infoLabel1 setFontSize:24.0];
        } else {
            [_infoLabel1 setFontSize:16.0];
        }
        
        [_infoLabel1 setPosition:CGPointMake(self.size.width/2, self.size.height/2+20)];
        [_infoLabel1 setFontColor:[UIColor darkGrayColor]];
        [_infoLabel1 setText:NSLocalizedString(@"inst_TapLeft", nil)];
        _infoLabel2 = [[SKLabelNode alloc] initWithFontNamed:@"Corbert-Regular"];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            [_infoLabel2 setFontSize:24.0];
        } else {
            [_infoLabel2 setFontSize:16.0];
        }
        
        [_infoLabel2 setPosition:CGPointMake(self.size.width/2, self.size.height/2-20)];
        [_infoLabel2 setFontColor:[UIColor darkGrayColor]];
        [_infoLabel2 setText:NSLocalizedString(@"inst_TapRight", nil)];
        [_infoNode addChild:_infoLabel1];
        [_infoNode addChild:_infoLabel2];
        [self addChild:_infoNode];
        
        _infoIconLeft = [SKSpriteNode spriteNodeWithImageNamed:@"TapLeft"];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            [_infoIconLeft setPosition:CGPointMake((self.size.width/2)-152, self.size.height/4)];
            [_infoIconLeft setScale:0.5];
        } else {
            [_infoIconLeft setPosition:CGPointMake((self.size.width/2)-76, self.size.height/4)];
            [_infoIconLeft setScale:0.25];
        }
        
        [self addChild:_infoIconLeft];
        
        _infoIconRight = [SKSpriteNode spriteNodeWithImageNamed:@"TapRight"];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            [_infoIconRight setPosition:CGPointMake((self.size.width/2)+152, self.size.height/4)];
            [_infoIconRight setScale:0.5];
        } else {
            [_infoIconRight setPosition:CGPointMake((self.size.width/2)+76, self.size.height/4)];
            [_infoIconRight setScale:0.25];
        }
        
        if (nightMode) {
            [_infoLabel1 setFontColor:[SKColor colorWithRed:.93 green:.93 blue:.93 alpha:1]];
            [_infoLabel2 setFontColor:[SKColor colorWithRed:.93 green:.93 blue:.93 alpha:1]];
        }
        
        _lPanel = [SKNode node];
        [_lPanel setPosition:CGPointMake(0, 0)];
        [self addChild:_lPanel];
        _lPanel.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(1, self.size.height)];
        [_lPanel.physicsBody setCategoryBitMask:kSideCategory];
        [_lPanel.physicsBody setCollisionBitMask:kPlayerCategory];
        [_lPanel.physicsBody setAffectedByGravity:NO];
        [_lPanel.physicsBody setDynamic:NO];
        [_lPanel setYScale:self.size.height];
        [_lPanel setName:@"noCheatPanel"];
        
        _rPanel = [SKNode node];
        [_rPanel setPosition:CGPointMake(self.size.width, 0)];
        [self addChild:_rPanel];
        _rPanel.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(1, self.size.height)];
        [_rPanel.physicsBody setCategoryBitMask:kSideCategory];
        [_rPanel.physicsBody setCollisionBitMask:kPlayerCategory];
        [_rPanel.physicsBody setAffectedByGravity:NO];
        [_rPanel.physicsBody setDynamic:NO];
        [_rPanel setYScale:self.size.height];
        [_rPanel setName:@"noCheatPanel"];
        
        [self addChild:_infoIconRight];
        
        //_pipeSound = [SKAction playSoundFileNamed:@"pipe.mp3" waitForCompletion:NO];
        _punchSound = [SKAction playSoundFileNamed:@"sound_Death.mp3" waitForCompletion:NO];
    }
    return self;
}

- (void)startGame {
    _pipeTimer = [NSTimer scheduledTimerWithTimeInterval:kPipeFrequency target:self selector:@selector(addObstacle) userInfo:nil repeats:YES];
    [NSTimer scheduledTimerWithTimeInterval:kPipeFrequency*2+0.1 target:self selector:@selector(startScoreTimer) userInfo:nil repeats:NO];
    
    //[self playSound];
}

- (void)playSound {
    if (hasPlayedIntro) {
        [self runAction:[SKAction playSoundFileNamed:@"TappinBG_Loop.mp3" waitForCompletion:NO] completion:^{
            
        }];
    } else {
        hasPlayedIntro = YES;
        [self runAction:[SKAction playSoundFileNamed:@"TappinBG_Intro.mp3" waitForCompletion:NO] completion:^{
            [self runLoopSound];
        }];
    }
}

- (void)runLoopSound {
    [NSTimer scheduledTimerWithTimeInterval:4.52 target:self selector:@selector(playSound) userInfo:Nil repeats:YES];
}

- (void)setupPlayer
{
    _player = [Player node];
    
    int playerColor = arc4random() % 5;
    switch (playerColor) {
        case 0:
            _player = [Player spriteNodeWithImageNamed:@"PlayerObj_Brown"];
            break;
        case 1:
            _player = [Player spriteNodeWithImageNamed:@"PlayerObj_Yellow"];
            break;
        case 2:
            _player = [Player spriteNodeWithImageNamed:@"PlayerObj_Blue"];
            break;
        case 3:
            _player = [Player spriteNodeWithImageNamed:@"PlayerObj_Orange"];
            break;
        case 4:
            _player = [Player spriteNodeWithImageNamed:@"PlayerObj_Purple"];
            break;
    }
    
    [_player setPosition:CGPointMake(self.size.width/2, self.size.height/4)];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [_player setScale:0.5];
    } else {
        [_player setScale:0.25];
    }
    
    [self addChild:_player];
    
    _player.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:_player.size];
    [_player.physicsBody setDensity:kDensity];
    [_player.physicsBody setDynamic:YES];
    [_player.physicsBody setAllowsRotation:NO];
    
    [_player.physicsBody setCategoryBitMask:kPlayerCategory];
    [_player.physicsBody setContactTestBitMask:kPipeCategory | kSideCategory];
    [_player.physicsBody setCollisionBitMask:kSideCategory | kPipeCategory];
}

- (void)addObstacle {
    CGFloat centerX = randomFloat(kPipeGap, self.size.width-kPipeGap);
    CGFloat pipeLeftHeight = centerX - (kPipeGap/2);
    CGFloat pipeRightHeight = self.size.width - (centerX + (kPipeGap/2));
    
    CGFloat nPipeWidth = kPipeWidth;
    if (_score > 600) {
        nPipeWidth = 49.5;
    } else if (_score > 500) {
        nPipeWidth = 50.0;
    } else if (_score > 400) {
        nPipeWidth = 51.5;
    } else if (_score > 300) {
        nPipeWidth = 52.0;
    } else if (_score > 200) {
        nPipeWidth = 52.5;
    } else if (_score > 100) {
        nPipeWidth = 53.0;
    }
    
    
    // Left Pipe
    Obstacle *pipeLeft = [Obstacle spriteNodeWithImageNamed:@"WallLeft"];
    [pipeLeft setCenterRect:CGRectMake(26.0/nPipeWidth, 26.0/nPipeWidth, 4.0/nPipeWidth, 4.0/nPipeWidth)];
    [pipeLeft setXScale:pipeLeftHeight/nPipeWidth];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [pipeLeft setYScale:0.75];
    }
    
    [pipeLeft setPosition:CGPointMake(pipeLeft.size.height/2-32, self.size.height + 20)];
    [self addChild:pipeLeft];
    
    pipeLeft.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:pipeLeft.size];
    [pipeLeft.physicsBody setAffectedByGravity:NO];
    [pipeLeft.physicsBody setDynamic:NO];
    
    [pipeLeft.physicsBody setCategoryBitMask:kPipeCategory];
    [pipeLeft.physicsBody setCollisionBitMask:kPlayerCategory];
    
    // Right Pipe
    Obstacle *pipeRight = [Obstacle spriteNodeWithImageNamed:@"WallRight"];
    [pipeRight setCenterRect:CGRectMake(26.0/nPipeWidth, 26.0/nPipeWidth, 4.0/nPipeWidth, 4.0/nPipeWidth)];
    [pipeRight setXScale:pipeRightHeight/nPipeWidth];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [pipeRight setYScale:0.75];
    }
    
    [pipeRight setPosition:CGPointMake(self.size.width - pipeRight.size.height/2+20, self.size.height + 20)];
    [self addChild:pipeRight];
    
    pipeRight.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:pipeRight.size];
    [pipeRight.physicsBody setAffectedByGravity:NO];
    [pipeRight.physicsBody setDynamic:NO];
    
    [pipeRight.physicsBody setCategoryBitMask:kPipeCategory];
    [pipeRight.physicsBody setCollisionBitMask:kPlayerCategory];
    
    // Move top pipe
    SKAction *pipeLeftAction;
    if (_score > 200) {
        pipeLeftAction = [SKAction moveToY:0 duration:kPipeSpeed-0.32];
    } else if (_score > 150) {
        pipeLeftAction = [SKAction moveToY:0 duration:kPipeSpeed-0.3];
    } else if (_score > 100) {
        pipeLeftAction = [SKAction moveToY:0 duration:kPipeSpeed-0.2];
    } else if (_score > 50) {
        pipeLeftAction = [SKAction moveToY:0 duration:kPipeSpeed-0.1];
    } else {
        pipeLeftAction = [SKAction moveToY:0 duration:kPipeSpeed];
    }
    
    SKAction *pipeLeftSequence = [SKAction sequence:@[pipeLeftAction, [SKAction runBlock:^{
        [pipeLeft removeFromParent];
    }]]];
    
    [pipeLeft runAction:[SKAction repeatActionForever:pipeLeftSequence]];
    
    // Move bottom pipe
    SKAction *pipeRightAction;
    
    if (_score > 200) {
        pipeRightAction = [SKAction moveToY:0 duration:kPipeSpeed-0.32];
    } else if (_score > 150) {
        pipeRightAction = [SKAction moveToY:0 duration:kPipeSpeed-0.3];
    } else if (_score > 100) {
        pipeRightAction = [SKAction moveToY:0 duration:kPipeSpeed-0.2];
    } else if (_score > 50) {
        pipeRightAction = [SKAction moveToY:0 duration:kPipeSpeed-0.1];
    } else {
        pipeRightAction = [SKAction moveToY:0 duration:kPipeSpeed];
    }
    
    SKAction *pipeRightSequence = [SKAction sequence:@[pipeRightAction, [SKAction runBlock:^{
        [pipeRight removeFromParent];
    }]]];
    
    [pipeRight runAction:[SKAction repeatActionForever:pipeRightSequence]];
}

- (void)startScoreTimer
{
    _scoreTimer = [NSTimer scheduledTimerWithTimeInterval:kPipeFrequency target:self selector:@selector(incrementScore) userInfo:nil repeats:YES];
}

- (void)incrementScore
{
    _score++;
    [_scoreLabel setText:[NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:_score]]];
    [self runAction:_pipeSound];
    
    if (_score == 1000000) {
        [self sendAchievementToGameCenter:kAchievementMillionPoints];
    }
    
    if (_score == 100) {
        [self sendAchievementToGameCenter:kAchievementCentury];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    
    if (!gameStarted) {
        gameStarted = YES;
        [_infoNode removeFromParent];
        [_infoIconRight removeFromParent];
        [_infoIconLeft removeFromParent];
        [self startGame];
    } else {
        switch ([touches count]) {
            case 1:
                if (touchLocation.x < (self.size.width / 2)) {
                    [_player runAction:[SKAction sequence:@[
                                                            [SKAction moveByX:-50 y:0 duration:0.1]
                                                            ]] completion:^{
                    }];
                } else {
                    [_player runAction:[SKAction sequence:@[
                                                            [SKAction moveByX:50 y:0 duration:0.1]
                                                            ]] completion:^{
                    }];
                }
                break;
            }
    }
}

- (void)sendAchievementToGameCenter:(NSString *)ach {
    [[GameCenterManager sharedManager] saveAndReportAchievement:ach percentComplete:100 shouldDisplayNotification:YES];
}

- (void)sendScoreToGameCenter {
    ViewController *vc = [[ViewController alloc] init];
    [vc submitScore:[[NSUserDefaults standardUserDefaults] integerForKey:@"lastScore"]];
}

- (void)update:(NSTimeInterval)currentTime {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([[defaults stringForKey:@"useTiltMode"] isEqualToString:@"YES"]) {
        [self processUserMotionForUpdate:currentTime-10];
    }
}

- (void)didBeginContact:(SKPhysicsContact *)contact
{
    SKNode *sideNode = contact.bodyB.node;
    if ([[sideNode name] isEqualToString:@"noCheatPanel"]) {
        NSLog(@"Player tried to cheat!");
    } else {
        [[NSUserDefaults standardUserDefaults] setInteger:_score forKey:@"lastScore"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        SKNode *node = contact.bodyA.node;
        if ([node isKindOfClass:[Player class]]) {
            [_pipeTimer invalidate];
            [_scoreTimer invalidate];
            [self runAction:_punchSound completion:^{
                SKTransition *transition = [SKTransition pushWithDirection:SKTransitionDirectionDown duration:.8];
                GameOverScene *newGame = [[GameOverScene alloc] initWithSize:self.size];
                
                [self.scene.view presentScene:newGame transition:transition];
            }];
        }
    }
}

-(void)processUserMotionForUpdate:(NSTimeInterval)currentTime {
    CMAccelerometerData* data = self.motionManager.accelerometerData;
    if (fabs(data.acceleration.x) > 0.2) {
        [_player.physicsBody applyForce:CGVectorMake(120 * data.acceleration.x, 0)];
    }
}

@end
